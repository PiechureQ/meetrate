const API_CODES = require('./enums/responseCodes');
const ACCOUNT_TYPES = require('./enums/accountTypes');

let talk = require('../dbModel/talk');
let event = require('../dbModel/event');
let Model = require('../dbModel/Model');


module.exports = {

    getEvents: (req, res, next) => {//TODO srednia ocena if skonczone
        Model(event).getAll().select({location: 1, date: 1, name: 1, status: 1, feedback: 1, talks:1}).populate('talks').exec(function (err, events) {
            if (events) {
                var events = JSON.parse(JSON.stringify(events));
                events.forEach((event) =>{
                    if (new Date(event.date.start) > Date.now()) { event.status = "future"; }
                    else if (new Date(event.date.end) < Date.now()) { event.status = "archive"; }
                    else { event.status = "actual" };
                    event.date.start = parseInt(new Date(event.date.start)/1000);
                    event.date.end = parseInt(new Date(event.date.end)/1000);

                    var avg = 0;

                    var i = 0;
                   event.feedback.forEach((feedback) =>{
                       avg += feedback.rating;
                       i++;
                   });
                    event.feedback = undefined;
                    event.feedback = {avg: avg/i, count: i};
                    var avg = 0;
                    var i = 0;
                   event.talks.forEach((talk) => {
                       talk.feedback.forEach((feedback) => {
                           avg += feedback.rating;
                           i++;
                       })
                   });
                    event.talks = undefined;
                    event.feedbackTalks = {avg: avg/i, count: i};
                });
                events.sort(sortByDate);
                events.push({serverTime: parseInt(Date.now()/1000)});
                res.status(API_CODES.OK).jsonp(events);
            } else {
                res.status(API_CODES.NOT_FOUND).send();
            }
        });
    },

    getEvent: (req, res, next) => { //TODO srednia ocen
            if(req.params.eventId){
                Model(event).getDocById(req.params.eventId).populate({path: 'talks', model:'talks', populate:{path: 'speakers', select: "name", model:'users'}}).exec((err, event) => {
                    if(event && !event.deleted){
                        var event = JSON.parse(JSON.stringify(event));
                        if (new Date(event.date.start) > Date.now()) { event.status = "future"; }
                        else if (new Date(event.date.end) < Date.now()) { event.status = "archive"; }
                        else { event.status = "active" };
                        event.date.start = parseInt(new Date(event.date.start)/1000);
                        event.date.end = parseInt(new Date(event.date.end)/1000);
                        var avg = 0;
                        var i = 0;
                        event.feedback.forEach((feedback) =>{
                            feedback.date = parseInt(new Date(feedback.date)/1000);
                            avg += feedback.rating;
                            i++;
                        });
                        event.feedback.push({avg: avg/i, count: i});
                        var talks =[];
                        event.talks.forEach((talk) => {
                            var talk = JSON.parse(JSON.stringify(talk));
                            talk.date.start = parseInt(new Date(talk.date.start)/1000);
                            talk.date.end = parseInt(new Date(talk.date.end)/1000);
                            var avg = 0;
                            var i = 0;
                            talk.feedback.forEach((feedback) => {
                                avg += feedback.rating;
                                i++;
                            });
                            talk.avg = {avg: avg/i, count: i};
                            talks.push(talk);
                        });
                        event.talks = talks;
                        var data = JSON.stringify(event);
                        res.json(event);
                    } else {
                        res.status(API_CODES.NOT_FOUND).send();
                    }
                });
            } else res.status(API_CODES.BAD_REQUEST).send();
    },

    getEventByUserId: (req, res, next) => {
        if(!req.params.userId){
            res.status(API_CODES.BAD_REQUEST).send()
        }else{
            Model(event).getDocByUserId(req.params.userId).exec((err, events) => {
                if(events){
                    var events = JSON.parse(JSON.stringify(events));
                    events.forEach((event) =>{
                        if (new Date(event.date.start) > Date.now()) { event.status = "future"; }
                        else if (new Date(event.date.end) < Date.now()) { event.status = "archive"; }
                        else { event.status = "actual" };
                        event.date.start = new Date(event.date.start)/1000;
                        event.date.end = new Date(event.date.end)/1000;
                        var avg = 0;

                        var i = 0;
                        event.feedback.forEach((feedback) =>{
                            avg += feedback.rating;
                            i++;
                        });
                        event.feedback = undefined;
                        event.feedback = {avg: avg/i, count: i};

                    });
                    events.sort(sortByDate);
                    events.push({serverTime: parseInt(Date.now()/1000)});
                    res.status(API_CODES.OK).jsonp(events);
                } else res.status(API_CODES.NOT_FOUND).send();
            })
        }
        },

    addEvent: (req, res, next) => {
        console.log(req.body);
        if(req.body.name && req.body.img && req.body.date && req.body.location && req.body.info){
            var e = new event({
                admin: req.session.userId,
                name: req.body.name,
                img: req.body.img,
                location: req.body.location,
                date: {
                    start: req.body.date.start * 1000,
                    end: req.body.date.end * 1000
                },
                info: req.body.info
            });
            Model(event).add(e).then((doc) => {
                res.status(API_CODES.CREATED).json(doc);
            });
        } else res.status(API_CODES.BAD_REQUEST).send();
    },

    updateEvent: (req, res, next) => {
        

        if (req.params.eventId) {
            Model(event).getDocById(req.params.eventId).exec(function (err, doc) {
                if (doc) {
                    if (req.body.name) {
                        doc.name = req.body.name;
                    }
                    if (req.body.date) {
                        doc.date.start = req.body.date.start * 1000;
                        doc.date.end = req.body.date.end * 1000;
                    }
                    if (req.body.location) {
                        doc.location = req.body.location;
                    }
                    if (req.body.img) {
                        doc.img = req.body.img;
                    }
                    if (req.body.info) {
                        doc.info = req.body.info;
                    }
                    if (req.body.talk) {
                        var duplicated;
                        doc.talks.forEach((t) => {
                            if (t.toString() === req.body.talk) {
                                duplicated = true;
                            }
                        });
                        if (!duplicated)
                            doc.talks.push(req.body.talk);
                    }
                    Model(event).save(doc);
                    res.status(API_CODES.OK).json(doc);
                } else {
                    res.status(API_CODES.NOT_FOUND).send();
                }
            })
        } else {
            res.status(API_CODES.BAD_REQUEST).send();
        }
    },

    leaveFeedback: (req, res, next) => {
        if(req.params.eventId && req.body.rating) {
            Model(event).getDocById(req.params.eventId).exec(function (err, doc) {
                if (doc && !doc.deleted) {

                    var e = JSON.parse(JSON.stringify(doc));
                    if (new Date(e.date.start) > Date.now()) { e.status = "future"; }
                    else if (new Date(e.date.end) < Date.now()) { e.status = "archive"; }
                    else { e.status = "active" };

                    if(e.status == "active") {

                    doc.feedback.push({
                        date: Date.now(),
                        rating: req.body.rating,
                        content: req.body.content
                    });
                    Model(event).save(doc);
                    res.status(API_CODES.CREATED).json(doc);

                    } else res.status(API_CODES.BAD_REQUEST).send();
                } else {
                    res.status(API_CODES.NOT_FOUND).send();
                }
            });
        } else {
            res.status(API_CODES.BAD_REQUEST).send()
        }
    },

    deleteEvent: (req, res, next) => {
        if(!req.params.eventId)  { res.status(API_CODES.BAD_REQUEST); return; }
        Model(event).getDocById(req.params.eventId).exec((err, doc) => {
            if(err) dberr(err, res);
            if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
            if(doc.admin == req.session.userId || req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN) {
                doc.deleted = true;
                doc.talks.forEach((talkId) => {
                    Model(talk).getDocById(talkId).exec((err, doc1) => {
                        if(doc1) {
                            doc1.deleted = true;
                            Model(talk).save(doc1);
                        }
                    });
                });
                Model(event).save(doc).then((doc) => {
                    res.status(API_CODES.OK).send();
                })
            } else res.send(API_CODES.FORBIDDEN).send();
        })
    },

    unassignTalk: (req, res, next) => {
        if(!req.params.eventId && !req.params.talkId)  { res.status(API_CODES.BAD_REQUEST); return; }
        Model(event).getDocById(req.params.eventId).exec((err, doc) => {
            if(err) dberr(err, res);
            if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
            if(doc.admin == req.session.userId || req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN) {
                event.findByIdAndUpdate(doc._id, {$pull:{talks: req.params.talkId}}, {new:1}, (err, doc2) => {
                    if(err) dberr(err, res);
                    if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
                    Model(talk).getDocById(req.params.talkId).exec((err, doc3) => {
                        if(err) dberr(err, res);
                        if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
                        talk.event = undefined;
                        Model(talk).save(talk);
                        res.status(API_CODES.OK).send();
                    });
                })
            } else res.send(API_CODES.FORBIDDEN).send();
        })
    },

    queryLocation: (req, res, next) => {
        Model(event).getAll().select('location').exec((err, doc) => {
            if(req.query.filter){
                var response = [];
                doc.forEach((item) => {
                    if(item.location.city.search(req.query.filter) != -1 || item.location.place.search(req.query.filter) != -1) {
                        response.push(item);
                   }
                });
                res.status(API_CODES.OK).json(deleteDuplicates(response));
            } else res.status(API_CODES.OK).json(doc);
        });
    }
};


function deleteDuplicates(arr) {
    arr = arr.filter((thing, index, self) =>
        index === self.findIndex((t) => (
            t.location.place === thing.location.place && t.location.city === thing.location.city
        ))
    );
    return arr;
}

function sortByDate(a,b) {
    if (a.date.start < b.date.start)
        return -1;
    if (a.date.start > b.date.start)
        return 1;
    return 0;
}

function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}
