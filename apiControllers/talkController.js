const API_CODES = require('./enums/responseCodes');
const ACCOUNT_TYPES = require('./enums/accountTypes');
var mongoose = require('mongoose');

let talk = require('../dbModel/talk');
let user = require('../dbModel/user');
let event = require('../dbModel/event');
let Model = require('../dbModel/Model');

module.exports = {
    getTalk: (req, res, next) => {
        if (req.params.talkId) {
            Model(talk).getDocById(req.params.talkId).populate('speakers').populate('event').exec((err, talk) => {
                if (talk && !talk.deleted) {
                    tal = JSON.parse(JSON.stringify(talk));
                    tal.date.start = parseInt(new Date(tal.date.start)/1000);
                    tal.date.end = parseInt(new Date(tal.date.end)/1000);
                    tal.event.date.start = parseInt(new Date(tal.event.date.start)/1000);
                    tal.event.date.end = parseInt(new Date(tal.event.date.end)/1000);
                    res.status(API_CODES.OK).json(tal);
                } else {
                    res.status(API_CODES.NOT_FOUND).send();
                }
            });
        } else res.status(API_CODES.BAD_REQUEST).send();
    },

    getTalkByEventId: (req, res, next) => {
        Model(talk).getDocByEventId(req.params.eventId).exec(function (err, talks) {
            if (talks) {
                var tal = JSON.parse(JSON.stringify(talks));
                tal.forEach((talk) => {
                    talk.date.start = parseInt(new Date(talk.date.start)/1000);
                    talk.date.end = parseInt(new Date(talk.date.end)/1000);
                });
                res.status(API_CODES.OK).json(tal);
            } else {
                res.status(API_CODES.NOT_FOUND).send();
            }
        });
    },

    getTalkByUserId: function (req, res, next) {
        if (!req.params.userId) {
            res.status(API_CODES.BAD_REQUEST).send();
            return;
        } else {
            Model(talk).getTalkByUserId(req.params.userId).exec((err, talk) => {
                if (talk) {
                    var talks = JSON.parse(JSON.stringify(talk));
                    talks.forEach((item) =>{
                    item.date.start = parseInt(new Date(item.date.start)/1000);
                    item.date.end = parseInt(new Date(item.date.end)/1000);
                });
                    res.status(API_CODES.OK).json(talks);
                } else res.status(API_CODES.NOT_FOUND).send();
            })
        }
    },

    addTalk: (req, res, next) => {
        if (req.body.eventId && req.body.title && req.body.date) {
            Model(event).getDocById(req.body.eventId).exec((err, doc) => {
                if(doc && !doc.deleted){
                    if(doc.admin == req.session.userId || req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN) {
                        var id = mongoose.Types.ObjectId();
                        var t = new talk({
                            _id: id,
                            event: req.body.eventId,
                            title: req.body.title,
                            date: {
                                start: req.body.date.start * 1000,
                                end: req.body.date.end * 1000
                            }
                        });
                        if (req.body.speakers) {
                            //remove duplicates
                            let speakers = req.body.speakers.filter(function(item, pos) {
                                return req.body.speakers.indexOf(item) === pos;
                            });

                            speakers.forEach((s) => {
                                t.speakers.push(s);
                                Model(user).getDocById(s).exec((err, doc2) => {
                                    if (err) dberr(err, res);
                                    if (!doc2) {
                                        res.status(API_CODES.NOT_FOUND).send();
                                        return;
                                    }
                                    doc2.talks.push(id);
                                    Model(user).save(doc2);})
                            });
                        }
                        Model(talk).add(t).then((doc2) => {
                            res.status(API_CODES.CREATED).json(doc2);
                            doc.talks.push(doc2._id);
                            Model(event).save(doc);
                        });
                    } else res.status(API_CODES.FORBIDDEN).send();
                } else res.status(API_CODES.NOT_FOUND).send();
            });
        } else res.status(API_CODES.BAD_REQUEST).send();
    },

    updateTalk: function (req, res, next) {
    if (req.params.talkId) {
        Model(talk).getDocById(req.params.talkId).exec(function (err, doc) {
            if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
            if (req.body.title) {
                doc.title = req.body.title;
            }
            if (req.body.date) {
                doc.date.start = req.body.date.start * 1000;
                doc.date.end = req.body.date.end * 1000;
            }
            if (req.body.speakers) {
                let speakers = req.body.speakers.filter(function(item, pos) {
                    return req.body.speakers.indexOf(item) === pos;
                });

                doc.speakers = speakers;

                speakers.forEach((s) => {
                    Model(user).getDocById(s).exec((err, doc2) => {
                        if (err) dberr(err, res);
                        if (!doc2) {
                            res.status(API_CODES.NOT_FOUND).send();
                            return;
                        }if(doc2.talks) doc2.talks.push(req.params.talkId);
                        else doc2.talks = [ req.params.talkId ];
                        Model(user).save(doc2);
                    })
                });
            }
            if(req.body.eventId){
                Model(event).getDocById(req.body.eventId).exec((err, doc2) => {
                    if(err) dberr(err, res);
                    if(!doc2) { res.status(API_CODES.NOT_FOUND).send(); return; }
                    doc.event = req.body.eventId;
                    if('talks' in doc2)
                    doc2.talks.push(doc._id);
                    else
                        doc2.talks = [ doc._id ];
                    Model(event).save(doc2);
                });
            }
            Model(talk).save(doc);
            res.status(API_CODES.OK).json(doc);
        });
    } else {
        res.status(API_CODES.BAD_REQUEST).send();
    }
},

    deleteTalk: (req, res, next) => {
    if(!req.params.talkId)  { res.status(API_CODES.BAD_REQUEST); return; }
    Model(talk).getDocById(req.params.talkId).exec((err, doc) => {
        if(err) dberr(err, res);
        if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
            doc.deleted = true;
            Model(talk).save(doc).then((doc) => {
                res.status(API_CODES.OK).send();
            })
    })
},

    unassignSpeaker: (req, res, next) => {
    if(!req.params.talkId && !req.params.userId)  { res.status(API_CODES.BAD_REQUEST); return; }
    Model(talk).getDocById(req.params.talkId).exec((err, doc) => {
        if(err) dberr(err, res);
        if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
            talk.findByIdAndUpdate(doc._id, {$pull:{speakers: req.params.userId}}, {new:1}, (err, doc2) => {
                if(err) dberr(err, res);
                if(!doc2) { res.status(API_CODES.NOT_FOUND).send(); return; }
                user.findByIdAndUpdate(req.params.userId, {$pull:{talks: req.params.talkId}}, {new: 1}, (err, doc3) =>{
                    if(err) dberr(err, res);
                    if(!doc3) { res.status(API_CODES.NOT_FOUND).send(); return; }
                    res.status(API_CODES.OK).send();
                });

            });
    })
},

    markAsRead: (req, res, next) => {
    if(req.params.questionId){
        talk.find({'questions._id': req.params.questionId}, (err, doc) => {
            if(doc){
                doc[0].questions.forEach((item) => {
                    if(item._id == req.params.questionId) item.readed = true;
                });
                doc[0].save();
                res.status(API_CODES.OK).send();
            } else res.status(API_CODES.NOT_FOUND).send();
        });
    } else res.status(API_CODES.BAD_REQUEST).send();
},

    leaveQuestion: (req, res, next) => {
    if(req.params.talkId && req.body.question) {
        Model(talk).getDocById(req.params.talkId).populate('speakers').exec(function (err, doc) {
            if (doc && !doc.deleted) {
                var id = mongoose.Types.ObjectId();
                doc.questions.push({
                    _id: id,
                    date: Date.now(),
                    content: req.body.question
                });
                doc.speakers.forEach((item) => {
                   if(item.socketId) req.io.to(item.socketId).emit('newQ', JSON.stringify({date: Date.now(),
                       content: req.body.question,
                        _id: id}));
                });
                Model(talk).save(doc);
                res.status(API_CODES.CREATED).json(doc);
            } else {
                res.status(API_CODES.NOT_FOUND).send();
            }
        });
    } else {
        res.status(API_CODES.BAD_REQUEST).send()
    }
},

    leaveFeedback: (req, res, next) => {
    if(req.params.talkId && req.body.rating) {
        Model(talk).getDocById(req.params.talkId).populate('speakers').exec(function (err, doc) {
            if (doc && !doc.deleted) {
                var id = mongoose.Types.ObjectId();
                doc.feedback.push({
                    _id: id,
                    date: Date.now(),
                    rating: req.body.rating,
                    content: req.body.content
                });
                doc.speakers.forEach((item) => {
                    if(item.socketId) req.io.to(item.socketId). emit('newF', JSON.stringify({
                        date: Date.now(),
                        content: req.body.content,
                        rating: req.body.rating,
                        _id: id,
                    }));
                });
                Model(talk).save(doc);
                res.status(API_CODES.CREATED).json(doc);
            } else {
                res.status(API_CODES.NOT_FOUND).send();
            }
        });
    } else {
        res.status(API_CODES.BAD_REQUEST).send()
    }
}
};

function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}