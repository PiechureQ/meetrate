module.exports = {
    INVALID_NAME: 0,
    INVALID_NAME_MSG: "niepoprawna nazwa",
    INVALID_EMAIL: 1,
    INVALID_EMAIL_MSG: "niepoprawny adres email",
    INVALID_ACCOUNT_TYPE: 2,
    INVALID_ACCOUNT_TYPE_MSG: "niepoprawny format konta",
    INVALID_OBJECTID: 3,
    INVALID_OBJECTID_MSG: "niepoprawny objectid",
    INVALID_EVENT_NAME: 4,
    INVALID_IMG: 5,
    INVALID_IMG_MSG: "niepoprawny format zdjecia",
    INVALID_LOCATION: 6,
    INVALID_LOCATION_MSG: "niepoprawne dane lokalizacji",
    INVALID_LOCATION_MSG_BASE: "niekompletny obiekt lokalizacji",
    INVALID_LOCATION_LATLONG_MSG: 'złe dane lat long',
    INVALID_INFO: 7,
    INVALID_INFO_MSG: "niepoprawne informacje o evencie",
    INVALID_DATE: 8,
    INVALID_DATE_MSG: "niepoprawny format daty",
    INVALID_RATING: 9,
    INVALID_RATING_MSG: "niepoprawny format oceny",
    INVALID_RATING_CONTENT: 10,
    INVALID_RATING_CONTENT_MSG: "niepoprawny format opinii",
    INVALID_QUESTION: 11,
    INVALID_QUESTION_MSG: "niepoprawny format pytania",
    INVALID_TYPE: 12,
    INVALID_TYPE_MSG: "niepoprawny typ danych",
    INVALID_REQUEST_FORMAT: 13,
    INVALID_REQUEST_FORMAT_MSG: "request data format error"
};