var bcrypt = require('bcrypt');
var randomstring = require("randomstring");
var sendMail = require('../mailer/nodeMailer');

const API_CODES = require('./enums/responseCodes');
const ACCOUNT_TYPES = require('./enums/accountTypes');

let user = require('../dbModel/user');
let Model = require('../dbModel/Model')(user);

let ADMINMAIL = "wasylkiewicz.dawid@gmail.com";

let TESTNING = false;

module.exports = {
    getUser: (req, res, next) => {
        Model.getDocById(req.params.userId).exec((err, user) => {
            if(err) dberr(err, res);
            if(user && !user.deleted) res.status(API_CODES.OK).json(user);
            else res.status(API_CODES.NOT_FOUND).send();
        });
    },

    getUsers: (req, res, next) => {
        if(req.session.permission == ACCOUNT_TYPES.ADMIN){
            Model.getDocByUserId(req.session.userId).exec((err, users) => {
                if(err) dberr(err, res);
                if(users) res.status(API_CODES.OK).json(users.sort());
                else res.status(API_CODES.NOT_FOUND).send();
            })
        } else if(req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN){
            Model.getAll().exec((err, users) => {
                if(err) dberr(err, res);
                if(users) res.status(API_CODES.OK).json(users.sort());
                else res.status(API_CODES.NOT_FOUND).send();
            })
        }
    },

    addUser: (req, res, next) => {
        if(!(req.body.name && req.body.email && req.body.type)){ res.status(API_CODES.BAD_REQUEST).send(); return; }
        if (req.session.permission == ACCOUNT_TYPES.ADMIN) {
            if (req.body.type >= ACCOUNT_TYPES.ADMIN) res.status(API_CODES.UNAUTHORIZED).send();
            else addUser(req, res);
        } else {
            if (req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN) addUser(req, res);
            else res.status(API_CODES.BAD_REQUEST).send();
        }
            function addUser(req, res) {
                Model.getUserByEmail(req.body.email).exec((err, doc) => {
                    if(err) dberr(err, res);
                    if (doc) { res.status(API_CODES.CONFLICT).send(); return; }
                    var password = randomstring.generate(8);
                    if(!TESTNING) {
                        sendMail(req.body.email, password, 1, req.session.name, req.body.name, (err, info) => {
                            if (err) {
                                global.logger.error({errorType: "mail", time: new Date(), msg: err});
                                res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
                                return;
                            }
                            var salt = bcrypt.genSaltSync(10);
                            bcrypt.hash(password, salt, (err, hash) => {
                                var u = new user({
                                    admin: req.session.userId,
                                    name: req.body.name,
                                    email: req.body.email,
                                    type: req.body.type,
                                    password_hash: hashCode(hash),
                                    password_salt: salt,
                                    newUser: true,
                                });
                                Model.add(u).then((doc) => {
                                    res.status(API_CODES.CREATED).json(doc);
                                });
                            });

                        });
                    }
                    else{
                        if (err) { global.logger.error({errorType:"mail", time: new Date(), msg:err}); res.status(API_CODES.INTERNAL_SERVER_ERROR).send(); return;}
                        var salt = bcrypt.genSaltSync(10);
                        bcrypt.hash(password, salt, (err, hash) => {
                            var u = new user({
                                admin: req.session.userId,
                                name: req.body.name,
                                email: req.body.email,
                                type: req.body.type,
                                password_hash: hashCode(hash),
                                password_salt: salt,
                                newUser: true,
                            });
                            Model.add(u).then((doc) => {
                                doc.password_hash = hash
                                res.status(API_CODES.CREATED).json(doc);
                            });
                        });
                        }
                });
            }
    },

   // modifyUser: (req, res, next) => {
      //  if(!isUserAdmin(req, res)) { res.status(API_CODES.UNAUTHORIZED).send(); return; }
      //  if(req.body.name && req.body.email && req.body.password_hash && req.body.password_salt && req.body.type){
          //  userModel.updateUser(req.params.userId, req.body).exec((err, doc) => {
          //      if(err) dberr(err, res);
              //  else res.status(API_CODES.OK).send();
            //});
       // } else res.status(API_CODES.BAD_REQUEST).send();
   // },

    patchUser: (req, res, next) => {
        if(req.body.type > ACCOUNT_TYPES.SPEAKER && req.session.permission != ACCOUNT_TYPES.SUPER_ADMIN) { res.status(API_CODES.UNAUTHORIZED).send(); return; }
        if(req.params.userId == req.session.userId || req.session.permission >= ACCOUNT_TYPES.ADMIN){
            if(req.body.adminId) {res.status(API_CODES.FORBIDDEN).send(); return;}
            if(req.body.talks) { res.status(API_CODES.FORBIDDEN).send(); return; }
            if(req.body.type == 0) {res.status(API_CODES.BAD_REQUEST).json({errors: {code: 2, message: "niepoprawny format konta"}}); return;}
            if('deleted' in req.body) { res.status(API_CODES.BAD_REQUEST).send(); return;}
            if(req.body.password_hash) req.body.password_hash = hashCode(req.body.password_hash);

            if(req.body.email) {
                var email = req.body.email;
                Model.getDocById(req.params.userId).exec((err, doc) => {
                    if (doc && !(doc.email == email)) {
                        Model.getUserByEmail(req.body.email).exec((err, doc2) => {
                            if(doc2) { res.status(API_CODES.CONFLICT).send(); return; }
                            var emailUrl = randomstring.generate();
                            sendMail(email, emailUrl, 0, "", "", (status) => {
                                if(err) { res.status(API_CODES.INTERNAL_SERVER_ERROR).send(); return; }
                                    doc.set({
                                        newEmail: email,
                                        emailUrl: emailUrl
                                    });
                                    doc.save();
                            });
                        });
                    } else {res.status(API_CODES.NOT_FOUND).send(); return;}
                });
            }
            Model.getDocById(req.params.userId).exec((err, doc) => {
            if(req.body.oldpass && req.body.newpass && req.body.newsalt){
                   if(err) dberr(err, res);
                   if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
                   var oldpassHash = hashCode(req.body.oldpass);
                   if(oldpassHash == doc.password_hash){

                       doc.password_hash = hashCode(req.body.newpass);
                       doc.password_salt = req.body.newsalt;
                       doc.save();

                   } else { res.status(API_CODES.FORBIDDEN).send(); return; }

            }

            delete req.body.email;
                if(req.body.type &&(req.session.permission <= ACCOUNT_TYPES.SPEAKER)) { res.status(API_CODES.FORBIDDEN).send(); return; }
                if(!res.headersSent) {
                    Model.updateDoc(req.params.userId, req.body).exec((err, doc) => {
                        if (err) dberr(err, res);
                        else res.status(API_CODES.OK).json(doc);
                    });
                } else res.status(API_CODES.BAD_REQUEST).send()
            });
        } else { res.status(API_CODES.FORBIDDEN).send(); }

    },

    verifyEmail: (req, res, next) =>  {
    if(req.params.emailId){
        user.findOne({emailUrl:req.params.emailId}, (err, doc) => {
            if(doc){
                doc.email = doc.newEmail;
                doc.newEmail = undefined;
                doc.emailUrl = undefined;
                req.session.destroy();
                doc.save();
                res.redirect('/panel/login');
            } else res.render('404');
        });
    } else res.status(API_CODES.BAD_REQUEST).send();
},


getSalt: (req, res, next) => {
        if(!req.params.email){ res.status(API_CODES.BAD_REQUEST); return; }
        Model.getUserByEmail(req.params.email).select({password_salt: 1}).exec((err, doc) => {
            if(err) dberr(err, res);
            if(doc) res.status(API_CODES.OK).json({salt: doc.password_salt});
            else res.status(API_CODES.NOT_FOUND).send();
        });
    },

    deleteUser: (req, res, next)=>{
      if(!req.params.userId)  { res.status(API_CODES.BAD_REQUEST); return; }
      Model.getDocById(req.params.userId).exec((err, doc) => {
          if(err) dberr(err, res);
          if(!doc) { res.status(API_CODES.NOT_FOUND).send(); return; }
          else if(doc.admin == req.session.userId || req.session.permission == ACCOUNT_TYPES.SUPER_ADMIN){
              doc.deleted = true;
              Model.save(doc).then((doc) => {
                  res.status(API_CODES.OK).send();
              })
          } else res.status(API_CODES.FORBIDDEN).send()
      })
    },

    loginUser: (req, res, next) => {
        if(!(req.headers.email && req.headers.password_hash)) { res.status(API_CODES.BAD_REQUEST).send(); return; }
        Model.getUserByEmail(req.headers.email).exec((err, user) => {
            if(err) dberr(err, res);
            else if(!user) { res.status(API_CODES.NOT_FOUND).send(); return; }
            if(hashCode(req.headers.password_hash) == user.password_hash){
                req.session.name = user.name;
                req.session.email = user.email;
                req.session.type = user.type;
                req.session.userId = user._id;
                res.status(API_CODES.OK).json({
                    name: user.name,
                    email: user.email,
                    type: user.type,
                    userId: user._id,
                    newUser: user.newUser,
                });
            } else res.status(API_CODES.UNAUTHORIZED).send();
        });
    },

    logoutUser: (req, res, next) => {
        req.session.destroy();
        res.redirect('/');
    },

    sendContactMail: (req, res, next) => {
      if(req.body.email && req.body.content){

          sendMail(ADMINMAIL, {content: req.body.content, email: req.body.email}, 2, '', '', (err) => {
              if (err) {
                  global.logger.error({errorType: "mail", time: new Date(), msg: err});
                  res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
                  return;
              }

              res.status(200).send();
          });

      }  else res.status(API_CODES.BAD_REQUEST).send();
    },

    queryNames: (req, res, next) => {
        Model.getAll().select('_id name').exec((err, doc) =>{
            if(req.query.filter){
                var response = [];
                doc.forEach((name) => {
                    if(name.name.search(req.query.filter) != -1) response.push(name);
                });
                res.status(API_CODES.OK).json(response)
            } else res.status(API_CODES.OK).json(doc);
        });
    }
};


function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}


function hashCode(s){
    return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
}