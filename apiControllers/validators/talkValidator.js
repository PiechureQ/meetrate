var validator = require('validator');
var VALIDATION_ERROR = require('../enums/validatorErrors');
var API_CODES = require('../enums/responseCodes');
const isHTML = require('is-html');

module.exports = (req, res, next) => {
    var error = {errors: []};

    if (req.body.eventId) {
        if(!validator.isMongoId(String(req.body.eventId))){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.body.title){
        if(req.body.title.length > 200 || isHTML(req.body.title)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_NAME,
                message:VALIDATION_ERROR.INVALID_NAME_MSG
            });
        }
    }

    if(req.body.date){
        if (req.body.date.start && req.body.date.end) {
            if (new Date(req.body.date.start * 1000) < new Date(Date.now()) || new Date(req.body.date.end * 1000) <= new Date(req.body.date.start * 1000) || isNaN(req.body.date.start) || isNaN(req.body.date.end) || new Date(req.body.date.start * 1000) > new Date(Date.now() + 59616000 * 1000) || new Date(req.body.date.end * 1000) > new Date(Date.now() + 59616000 * 1000)) {
                error.errors.push({
                    code: VALIDATION_ERROR.INVALID_DATE,
                    message: VALIDATION_ERROR.INVALID_DATE_MSG
                });
            }
        } else {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_DATE,
                message: VALIDATION_ERROR.INVALID_DATE_MSG
            });
        }
    }

    if (req.body.rating){
        if (!validator.isInt(req.body.rating.toString(), {min:1, max:3})) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_RATING,
                message: VALIDATION_ERROR.INVALID_RATING_MSG
            });
        }
    }

    if (req.body.content){
        if (isHTML(req.body.content) || req.body.content.toString().length > 500) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_RATING_CONTENT,
                message: VALIDATION_ERROR.INVALID_RATING_CONTENT_MSG
            });
        }
    }

    if (req.body.question){
        if (req.body.question.length > 500 || isHTML(req.body.question)) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_QUESTION,
                message: VALIDATION_ERROR.INVALID_QUESTION_MSG
            });
        }
    }

    if (req.body.speakers) {
        if (Array.isArray(req.body.speakers)) {
            if (req.body.speakers.length > 0) {
                req.body.speakers.forEach((id) => {
                    if (!validator.isMongoId(String(id))) {
                        error.errors.push({
                            code: VALIDATION_ERROR.INVALID_OBJECTID,
                            message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
                        });
                    }
                });
            } else {
                error.errors.push({
                    code: VALIDATION_ERROR.INVALID_OBJECTID,
                    message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
                });
            }
        } else {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.eventId){
        if(!validator.isMongoId(req.params.eventId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.userId){
        if(!validator.isMongoId(req.params.userId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.questionId){
        if(!validator.isMongoId(req.params.eventId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.talkId){
        if(!validator.isMongoId(req.params.eventId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(error.errors.length > 0){
        res.status(API_CODES.BAD_REQUEST).json(error);
        return;
    }

    next();
};

