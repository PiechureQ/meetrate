var validator = require('validator');
var VALIDATION_ERROR = require('../enums/validatorErrors');
var API_CODES = require('../enums/responseCodes');
const isHTML = require('is-html');

module.exports = (req, res, next) => {
    var error = {errors:[]};

    if(req.body.name){
        if(isHTML(req.body.name) || req.body.name.length > 100){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_EVENT_NAME,
                message:VALIDATION_ERROR.INVALID_NAME_MSG
            });
        }
    }

    if(req.body.img) {
        if (!validator.isBase64(String(req.body.img))) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_IMG,
                message: VALIDATION_ERROR.INVALID_IMG_MSG
            });
        }
    }

    if(req.body.location){
        if (req.body.location.city && req.body.location.place && req.body.location.lat && req.body.location.lng) {
            if (isHTML(req.body.location.place) || isHTML(req.body.location.city) || req.body.location.city.length > 100 || req.body.location.place.length > 100) {
                error.errors.push({
                    code: VALIDATION_ERROR.INVALID_LOCATION,
                    message: VALIDATION_ERROR.INVALID_LOCATION_MSG
                });
            }
            if ((!validator.isLatLong(req.body.location.lat.toString() + "," + req.body.location.lng.toString())) || req.body.location.lat.toString().length > 30 || req.body.location.lng.toString().length > 30) {
                error.errors.push({
                    code: VALIDATION_ERROR.INVALID_LOCATION,
                    message: VALIDATION_ERROR.INVALID_LOCATION_LATLONG_MSG
                })
            }
        } else {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_LOCATION,
                message: VALIDATION_ERROR.INVALID_LOCATION_MSG_BASE
            })
        }
    }

    if(req.body.date){
        if (req.body.date.start && req.body.date.end) {
            if (new Date(req.body.date.start * 1000) < new Date(Date.now()) || new Date(req.body.date.end * 1000) <= new Date(req.body.date.start * 1000) || isNaN(req.body.date.start) || isNaN(req.body.date.end) || new Date(req.body.date.start * 1000) > new Date(Date.now() + 59616000 * 1000) || new Date(req.body.date.end * 1000) > new Date(Date.now() + 59616000 * 1000) || (new Date(req.body.date.end * 1000 ) - new Date(req.body.date.start * 1000) > 259200000)) {
                error.errors.push({
                    code: VALIDATION_ERROR.INVALID_DATE,
                    message: VALIDATION_ERROR.INVALID_DATE_MSG
                });
            }
        } else {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_DATE,
                message: VALIDATION_ERROR.INVALID_DATE_MSG
            });
        }
    }

    if (req.body.info) {
        if(isHTML(req.body.info) || req.body.info.toString().length > 500){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_INFO,
                message: VALIDATION_ERROR.INVALID_INFO_MSG
            });
        }
    }

    if (req.body.rating){
        if (!validator.isInt(req.body.rating.toString(), {min:1, max:5})) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_RATING,
                message: VALIDATION_ERROR.INVALID_RATING_MSG
            });
        }
    }

    if (req.body.content){
        if (isHTML(req.body.content) || req.body.content.toString().length > 500) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_RATING,
                message: VALIDATION_ERROR.INVALID_RATING_MSG
            });
        }
    }

    if (req.body.talk) {
        if(!validator.isMongoId(req.body.talk)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.userId){
        if(!validator.isMongoId(req.params.userId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(req.params.eventId){
        if(!validator.isMongoId(req.params.eventId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }

    if(error.errors.length > 0){
        res.status(API_CODES.BAD_REQUEST).json(error);
        return;
    }
    next();
};