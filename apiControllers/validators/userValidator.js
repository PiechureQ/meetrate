var validator = require('validator');
var VALIDATION_ERROR = require('../enums/validatorErrors');
var API_CODES = require('../enums/responseCodes');
const isHTML = require('is-html');

module.exports = (req, res, next) => {
    var error = {errors:[]};
    if(req.body.name){
        if(!(req.body.name.split(' ').length >= 2) || !validator.isAlpha(req.body.name.replace(/[^\w\s]/gi, '').replace(' ', ''), ["pl-PL"]) || req.body.name.length > 30 || isHTML(req.body.name)){
            error.errors.push({code: VALIDATION_ERROR.INVALID_NAME, message:VALIDATION_ERROR.INVALID_NAME_MSG});
        }
    }
    if(req.body.email) {
        req.body.email = req.body.email.toLowerCase();
        if (!validator.isEmail(req.body.email) || req.body.email.length > 45) {
            error.errors.push({code: VALIDATION_ERROR.INVALID_EMAIL, message:VALIDATION_ERROR.INVALID_EMAIL_MSG});
        }
    }
    if(req.body.type) {
        if (!validator.isInt(req.body.type.toString(), {min:1, max:3})) {
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_ACCOUNT_TYPE,
                message: VALIDATION_ERROR.INVALID_ACCOUNT_TYPE_MSG
            });
        }
    }
    if(req.body.newUser){
        if(!validator.isBoolean(req.body.newUser.toString())){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_ACCOUNT_TYPE,
                message: VALIDATION_ERROR.INVALID_ACCOUNT_TYPE_MSG
            });
        }
    }
    if(req.params.userId){
        if(!validator.isMongoId(req.params.userId)){
            error.errors.push({
                code: VALIDATION_ERROR.INVALID_OBJECTID,
                message: VALIDATION_ERROR.INVALID_OBJECTID_MSG
            });
        }
    }
    if(error.errors.length > 0){
        res.status(API_CODES.BAD_REQUEST).json(error);
        return;
    }
    next();
};
