module.exports = (req, res, next) => {
    var route = req.originalUrl.split('/');
    switch (route[3]) {
        case 'user':
            var validator = require('./userValidator');
            validator(req, res, next);
            break;
        case 'talk':
            var validator = require('./talkValidator');
            validator(req, res, next);
            break;
        case 'feedback':
            if (route[4] === 'event'){
                var validator = require('./eventValidator');
                validator(req, res, next);
            }
            if(route[4] === 'talk') {
                var validator = require('./talkValidator');
                validator(req, res, next);
            }
            break;
        case 'question':
            var validator = require('./talkValidator');
            validator(req, res, next);
            break;
        case 'event':
            var validator = require('./eventValidator');
            validator(req, res, next);
            break;
        default:
            next();
    }
};