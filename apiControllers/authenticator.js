var API_CODES = require('./enums/responseCodes');
var VALIDATION_ERROR = require('./enums/validatorErrors');
var ACCOUNT_TYPES = require('./enums/accountTypes');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/MeetRate');

let user = require('../dbModel/user');
let Model = require('../dbModel/Model')(user);

module.exports.auth = (req, res, next) => {
        if(req.session.userId){
            Model.getDocById(req.session.userId).exec((err, doc) =>{
                if(err) {
                    global.logger.error({errorType:"db", time: new Date(), msg:err});
                    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
                    return;
                } else if(doc){
                    req.session.permission = doc.type;
                    next()
                } else {
                    next()
                }
            })
        }else{
            req.session.permission = 0;
            next()
        }
};

module.exports.userRestricted = (req, res, next) => {
        if(!isUserAdmin(req, res)) { res.status(API_CODES.FORBIDDEN).send(); return; }
    next()
};

module.exports.visitorRestricted = (req, res, next) => {
    if(req.session.permission == ACCOUNT_TYPES.USER) { res.status(API_CODES.FORBIDDEN).send(); return; }
    next()
};

function isUserAdmin(req, res) {
    if(req.session.permission < ACCOUNT_TYPES.ADMIN || req.session.permission === undefined) {
        return false;
    } else return true;
}

function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}
