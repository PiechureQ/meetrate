var express = require('express');
var mongoose = require("mongoose");

mongoose.connect('mongodb://localhost/MeetRate');
var router = express.Router();

var event = require('../dbModel/event');
var Model = require('../dbModel/Model')(event);

router.get('/', (req, res, next) => {
   res.render('index');
});

router.get('/:eventId', (req, res, next) => {

    Model.getDocById(req.params.eventId).populate({path: 'talks', model:'talks', populate:{path: 'speakers', select: "name", model:'users'}}).exec((err, event) => {
        if(event && !event.deleted){
            var event = JSON.parse(JSON.stringify(event));
            console.log(new Date(event.date.start));
            console.log(Date.now().toString());
            if (new Date(event.date.start) > Date.now()) { event.status = "future"; }
            else if (new Date(event.date.end) < Date.now()) { event.status = "archive"; }
            else { event.status = "actual" };

            event.date.start = new Date(event.date.start)/1000;
            event.date.end = new Date(event.date.end)/1000;
            var avg = 0;
            var i = 0;
            event.feedback.forEach((feedback) =>{
                avg += feedback.rating;
                i++;
            });
            event.feedback = undefined;
            event.feedback = {avg: avg/i, count: i};
            var talks =[];
            var avgTalks =0;
            var j = 0;
            event.talks.forEach((talk) => {
                var talk = JSON.parse(JSON.stringify(talk));
                talk.date.start = parseInt(new Date(talk.date.start)/1000);
                talk.date.end = parseInt(new Date(talk.date.end)/1000);
                var avg = 0;
                var i = 0;
                talk.feedback.forEach((feedback) => {
                    avg += feedback.rating;
                    avgTalks += feedback.rating;
                    i++;
                    j++;
                });
                talk.avg = {avg: avg/i, count: i};
                talks.push(talk);
            });
            talks.sort(sortByDate);
            event.feedbackTalks = {avg: avgTalks/j, count: j};
            event.talks = talks;
            var data = JSON.stringify(event);
            res.render('event', {event: data});
        } else {
            res.render('404');
        }
    });
});

function sortByDate(a,b) {
    if (a.date.start < b.date.start)
        return -1;
    if (a.date.start > b.date.start)
        return 1;
    return 0;
}

function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}

module.exports = router;