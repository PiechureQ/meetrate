var express = require('express');
var mongoose = require("mongoose");

mongoose.connect('mongodb://localhost/MeetRate');

var router = express.Router();

let userController = require('../apiControllers/userController');
let eventController = require('../apiControllers/eventController');
let talkController = require('../apiControllers/talkController');

var authenticator = require('../apiControllers/authenticator');

router.get('/user', authenticator.userRestricted, userController.getUsers);
router.get('/user/:userId', authenticator.userRestricted, userController.getUser);
router.post('/user', authenticator.userRestricted, userController.addUser);
router.patch('/user/:userId', authenticator.visitorRestricted, userController.patchUser);
router.delete('/user/:userId', authenticator.userRestricted, userController.deleteUser);
router.get('/user/email/:emailId', userController.verifyEmail);

router.get('/login', userController.loginUser);
router.get('/login/:email', userController.getSalt);
router.get('/logout', userController.logoutUser);

router.get('/event', eventController.getEvents);
router.get('/event/:eventId', eventController.getEvent);
router.get('/event/user/:userId', authenticator.visitorRestricted, eventController.getEventByUserId);
router.post('/event', authenticator.userRestricted, eventController.addEvent);
router.patch('/event/:eventId', authenticator.userRestricted,  eventController.updateEvent);
router.patch('/event/:eventId/talk/:talkId', authenticator.userRestricted, eventController.unassignTalk);
router.delete('/event/:eventId', authenticator.userRestricted, eventController.deleteEvent);

router.get('/talk/event/:eventId', talkController.getTalkByEventId);
router.get('/talk/:talkId', talkController.getTalk);
router.get('/talk/user/:userId', authenticator.visitorRestricted, talkController.getTalkByUserId);
router.post('/talk', authenticator.userRestricted, talkController.addTalk);
router.patch('/talk/:talkId', authenticator.userRestricted, talkController.updateTalk);
router.patch('/talk/:talkId/user/:userId', authenticator.userRestricted, talkController.unassignSpeaker);
router.delete('/talk/:talkId', authenticator.userRestricted, talkController.deleteTalk);

router.post('/feedback/event/:eventId', eventController.leaveFeedback);
router.post('/feedback/talk/:talkId', talkController.leaveFeedback);

router.post('/question/talk/:talkId', talkController.leaveQuestion);
router.patch('/question/:questionId', authenticator.visitorRestricted, talkController.markAsRead);

router.get('/query/user',  authenticator.userRestricted, userController.queryNames);
router.get('/query/location', eventController.queryLocation);

router.post('/contact/sendMail', userController.sendContactMail);

module.exports = router;
