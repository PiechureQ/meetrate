var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/login', function(req, res, next) {
    if(req.session.userId)
        res.redirect('/panel/index');
    else res.render('login');
});

router.get('/index', function(req, res, next) {
    if(req.session.userId) {
            var type;
            switch(req.session.type) {
                case 1:
                    type = "Prelegent";
                    break;
                case 2:
                    type = "Admin";
                    break;
                case 3:
                    type = "Admin";
            }
            res.render('speakerPanel.ejs', {ac: {name: req.session.name, type: type, email: req.session.email, userId: req.session.userId}});
        } else res.redirect('/panel/login');
});


function dberr(err, res) {
    global.logger.error({errorType:"db", time: new Date(), msg:err});
    res.status(API_CODES.INTERNAL_SERVER_ERROR).send();
    return;
}

module.exports = router;
