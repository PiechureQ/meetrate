# MeetRate
MeetRate is an aplication for showing and planing events.

This repository is a project for which I worked during an intern at RST Software Masters in the period 2.07.18 - 3.07.18. My contribution in this project is the implementation and development of the backend (REST API, Database structure, Operations on the database).

## Technologies

[Mongo](https://www.mongodb.com/) for a NoSQL database.

[Express](https://expressjs.com/) For an HTTP Server

[Node](https://nodejs.org/en/) For a JavaScript runtime