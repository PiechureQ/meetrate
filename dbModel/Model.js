module.exports = (db) => {
  return {
      getAll: () => {
          return db.find({deleted: {$exists: false}});
      },
      getDocById: (id) => {
          return db.findById({_id: id, deleted: {$exists: false}});
      },
      getDocByUserId: (userId) => {
          return db.find({admin: userId, deleted: {$exists: false}})
      },
      getDocByEventId: (eventId) => {
          return db.find({event: eventId, deleted: {$exists: false}})
      },
      getTalkByUserId: (userId) => {
          return db.find({speakers: userId, deleted: {$exists: false}})
      },
      getUserByEmail: (userEmail) => {
          return db.findOne({email: userEmail, deleted: {$exists: false}});
      },
      add: (doc) => {
          return doc.save()
      },
      save: (doc) => {
          return doc.save()
      },
      updateDoc: (id, doc) => {
          return db.findByIdAndUpdate(id, {$set: doc}, {new: 1});
      },
  }
};