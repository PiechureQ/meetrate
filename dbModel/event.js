var mongoose = require('mongoose');

var feedbackSchema = mongoose.Schema({
    date: Date,
    rating: Number,
    content: String
});

var eventSchema = mongoose.Schema({
    admin: {type:mongoose.Schema.Types.ObjectId, ref:'users'},
    name: String,
    img: String,
    location: {
        city: String,
        place: String,
        lat: Number,
        lng: Number
    },
    date: {
        start: Date,
        end: Date
    },
    info: String,
    talks: [{type:mongoose.Schema.Types.ObjectId, ref:'talks'}],
    feedback: [feedbackSchema],
    deleted: Boolean,
});



module.exports = mongoose.model('events', eventSchema);