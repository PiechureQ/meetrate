var mongoose = require('mongoose');

var userSchema = {
    admin: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
    name: String,
    email: String,
    password_hash: String,
    password_salt: String,
    type: Number,
    talks: [{type: mongoose.Schema.Types.ObjectId, ref: 'talks'}],
    newUser: Boolean,
    deleted: Boolean,
    socketId: String,
    newEmail: String,
    emailUrl: String,
};


module.exports = mongoose.model('users', userSchema);