var mongoose = require('mongoose');

var feedbackSchema = mongoose.Schema({
    date: Date,
    rating: Number,
    content: String
});


var talksSchema = {
    event: {type:mongoose.Schema.Types.ObjectId, ref:'events'},
    title: String,
    date: {
        start: Date,
        end: Date
    },
    speakers: [{type: mongoose.Schema.Types.ObjectId, ref: 'users'}],
    feedback: [feedbackSchema],
    questions: [{date: Date, content: String, readed: Boolean}],
    deleted: Boolean,
};

module.exports = mongoose.model('talks', talksSchema);