var express = require('express');
var socket_io = require('socket.io');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var mongoose = require('mongoose');
var MongoStore = require('connect-mongo')(session);

var winston = require('winston');

mongoose.connect('mongodb://localhost/MeetRate');

var userDb = require('./dbModel/user');
var Model = require('./dbModel/Model')(userDb);

global.logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});

var index = require('./routes/index');
var panel = require('./routes/speakerPanel');

var api = require('./routes/api');


var app = express();
var io = socket_io();
app.io = io;

app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: "10mb"}));


app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'seems legit',
    resave: true,
    saveUninitialized: true,
    cookie: { httpOnly: false },
    store: new MongoStore({url: 'mongodb://localhost/MeetRate'})}));

app.use((err, req, res, next) => {
    if (err) {
        res.status(400).send();
        return;
    } else {
        next()
    }
});

app.use((req, res, next) => {
    req.io = io;
    if(req.session.userId && req.originalUrl == "/panel/index") {
        io.on('connection', (socket) => {
            Model.getDocById(req.session.userId).exec((err, doc) => {
                doc.socketId = socket.id;
                Model.save(doc);
            });
        });
        next()
    } else next();
});


var authenticator = require('./apiControllers/authenticator').auth;
var validator = require('./apiControllers/validators/validator');


app.use('/', index);
app.use('/panel', panel);

app.use('/api/v1', authenticator, validator, api);

app.get('*', function(req, res){
    res.render('404');
});

module.exports = app;
