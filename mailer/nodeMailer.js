var nodeMailer = require('nodemailer');
var mailOption = require('./mailOptions.js');

module.exports = (email, url, emailType, owner, name, callback) => {

    var transporter = require('./nodeMailerTransportConfig');

    transporter.sendMail(mailOption(email, url, emailType, owner, name), (error, info) => {
        callback(error, info);
    });
};