var nodeMailer = require('nodemailer');
var fs = require('fs');
var path = require('path');
var ejs = require('ejs');

const host = 'dev-vote.rst.com.pl';

module.exports = (email, url, emailType, owner, name) => {
    switch (emailType){
        case 0:
            var emailTemplate = fs.readFileSync(path.join(__dirname, 'emailBody.ejs'), 'utf-8');
            var url1 = "http://" + host + '/api/v1/user/email/' + url;

            var text = 'Hello world, \n Your verification url \n' + url1;
            var html = ejs.render(emailTemplate, {url: url1});
            return {
                from: '"no-reply" <vayned22@gmail.com>', // sender address
                to: email, // list of receivers
                subject: 'Email Verification', // Subject line
                text: text, // plain text body
                html:  html// html body
            };
            break;
        case 1:
            var passwordTemplate = fs.readFileSync(path.join(__dirname, "passwordBody.ejs"), 'utf-8');
            var text = 'Hello world, \n Your verification url \n' + url1;
            var html = ejs.render(passwordTemplate, {url: url1, owner: owner, email:email, password: url, name: name});
            return {
                from: '"no-reply" <vayned22@gmail.com>', // sender address
                to: email, // list of receivers
                subject: 'Registration', // Subject line
                text: text, // plain text body
                html:  html// html body
            };
            break;
        case 2:
            var contactTemplate = fs.readFileSync(path.join(__dirname, "contactBody.ejs"), 'utf-8');
            var text = '';
            var html = ejs.render(contactTemplate, {name: name, email: url.email, content: url.content});
            return {
                from: '"MeetRate" <vayned22@gmail.com>',
                to: email,
                subject: 'MeetRate - zgłoś się',
                text: text,
                html: html,
            };
    }
};
