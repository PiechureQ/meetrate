$('#changeemail').click(() => {
    var data = {
        email: $('#newemail').val(),
        email2: $('#newemail2').val()
    };
    var id = $('#changeemail').attr("class");

    if (!(data.email === data.email2)){
        $('#changeemailer').css('display', 'block').text("Adresy nie są identyczne!");
    } else if (!validateEmail(data.email) && !validateEmail(data.email2)){
        $('#changeemailer').css('display', 'block').text("Zły format e-maila!");
    } else {
        $.ajax({
            type: "PATCH",
            url: "/api/v1/user/" + id,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            statusCode: {
                200: () => {
                    $('#changeEmailModal > .modal-dialog > .modal-content > .modal-body').html("Link weryfikacyjny został wysłany na nowy adres email.");
                },
                409: () => {
                    $('#changeemailer').css('display', 'block');
                },
                400: () => {
                }
            },
        });
    }
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}