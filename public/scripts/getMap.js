function createMap(lat, lng) {

  var myCenter = new google.maps.LatLng(lat,lng),
      mapCanvas = document.getElementById("map");

  var mapOptions = {
      center: myCenter,
      zoom: 15,
      disableDefaultUI: true,
      zoomControl: true,
      styles:
      [
          {
            "featureType": "landscape.man_made",
            "stylers": [
              {
                "color": "#808d8e"
              }
            ]
          },
          {
            "featureType": "landscape.man_made",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "landscape.man_made",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "landscape.natural",
            "stylers": [
              {
                "color": "#e6e8e8"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#808d8e"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.icon",
            "stylers": [
              {
                "saturation": -100
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#515757"
              }
            ]
          },
          {
            "featureType": "poi.attraction",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry.fill",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#808d8e"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#a3e3c7"
              }
            ]
          }
        ],
      mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  var marker = new google.maps.Marker({
      position: myCenter,
      icon:'img/icon-marker.png'
  });

  var map = new google.maps.Map(mapCanvas, mapOptions);

  marker.setMap(map);

  google.maps.event.addListener(marker,'click',function() {
    map.setZoom(17);
    map.setCenter(marker.getPosition());
  });


}
