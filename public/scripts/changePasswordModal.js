
$('#changepass').click(() => {

    var oldpass = $('#oldpass').val();
    var newpass = $('#newpass').val();
    var newpass2 = $('#newpass2').val();
    var email = $('#changepass').attr('class').split(' ')[1];
    var id = $('#changepass').attr('class').split(' ')[0];

    var newsalt = gensalt(10);
    if (oldpass === newpass) {
        $('#changepasser').css('display', 'block').text("Nowe oraz stare hasło nie może być identyczne!");
    } else if (!(newpass === newpass2)) {
        $('#changepasser2').css('display', 'block').text("Hasła nie są identyczne");
    } else {
        $('#changepasser').css('display', 'none');
        $('#changepasser2').css('display', 'none');
        $.ajax({
            type: "GET",
            url: "/api/v1/login/" + email,
            contentType: "application/json; charset=utf-8",
            statusCode: {
                200: (data) => {
                    hashpw(oldpass, data.salt, (oldHash) => {
                        hashpw(newpass, newsalt, (newHash) => {
                            var data = {
                                oldpass: oldHash,
                                newpass: newHash,
                                newsalt: newsalt
                            };

                            $.ajax({
                                type: "PATCH",
                                url: "/api/v1/user/" + id,
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                statusCode: {
                                    200: () => {
                                        $('#changePasswordModal > .modal-dialog > .modal-content > .modal-body').html("Gratulacje użytkowniku, twoje hasło zostało poprawnie zmienione. :)");
                                    },
                                    403: () => {
                                        $('#changepasser').css('display', 'block').text("Złe hasło");
                                    }
                                },
                            });

                        })
                    })
                }
            }
        });
    }
});
