﻿
function sendData(data, url, succes, error){

    url = "api/v1/" + url;
        console.log(data);
        $.ajax({
            method: "POST",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            processData: false,
            data: JSON.stringify(data),
            statusCode: {
                201: (data) => {
                    succes(data.responseText);
                },
                400: (data) => {
                    error(data.responseText);
                },
                404: (data) => {
                    error(data.responseText);
                },
                405: (data) => {
                    error(data.responseText);
                },
            },
        });
 
 }
 
 function rateEvent(id) {
 
    var rate = $("#eventRate input[name='eventRate']:checked").val();
    var opininon = $('#eventRate textarea').val().trim();

    var url = "feedback/event/" + id;

    var data = {
        rating: rate,
        content: opininon,
    }

    if(rate){

        sendData(data, url, askQuestionSucces, askQuestionErr);
        $("#resultsWindow h1").not("ol h1").css({"color": "var(--gray)", "text-decoration": "none"});


    } else {

        createPrompt("sad", "Proszę wystawić ocenę.");
        $("#eventRate h1:first-of-type").not("ol").css({"color": "var(--red)", "text-decoration": "underline"});

    }

 }
 
 function askSpeaker(id){
 
     var opininon = $("#" + id + " .askQuestion textarea").val().trim();
 
     var url = "question/talk/" + id;
 
     var data = {
        question: opininon
     }
 
     if(opininon){
 
        sendData(data, url, askQuestionSucces, askQuestionErr);
        $("#resultsWindow .askQuestion h1").css({"color": "var(--gray)", "text-decoration": "none"});
 
     } else {
 
        createPrompt("sad", "Proszę uzupełnić pole tekstowe.");
        $("#" + id + " .askQuestion h1:first-of-type").css({"color": "var(--red)", "text-decoration": "underline"});
 
     }
 
 }
 
 function rateSpeaker(id) {
 
     var rate = $("#" + id + " .giveRate input[name='speakerRate']:checked").val();
     var opininon = $("#" + id + " .giveRate textarea").val().trim();
     
     var url = "feedback/talk/" + id;
 
     var data = {
        rating: rate
     }

     if(opininon) data.content = opininon;
 
     if(rate){
 
        sendData(data, url, askQuestionSucces, askQuestionErr);
        $("#resultsWindow .giveRate h1").css({"color": "var(--gray)", "text-decoration": "none"});
 
     } else {
 
        createPrompt("sad", "Proszę wystawić ocenę.");
        $("#" + id + " .giveRate h1:first-of-type").css({"color": "var(--red)", "text-decoration": "underline"});
 
     }
 
 }
 
 function askQuestionErr(data) {
    createPrompt("blase", "Błąd serwera!");
 }
 
 function askQuestionSucces(data) {
     createPrompt("happy", "Dziękujemy!");
 }

 function createPrompt(emotions, content){
 
     var time = 2000;
 
     $("#promptQueue").append('<div class="prompt ' + emotions + 'Prompt">' + content + '</div>');
     setTimeout( function(){ 
        $("#promptQueue .prompt:first-child").remove();
         $(".submit").prop("disabled", false); 
         $("input[type='radio']").prop("disabled", false);
         $("textarea").prop("disabled", false);
     } ,time);
 
 }