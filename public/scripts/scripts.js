
$( document ).ready(function() {


    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $("body").html('<div style=" display: flex; align-items: center; justify-content: center; flex-direction: column; height: 100vh;"><img src="../img/logo-sygnet.svg" style="height: 64px; text-align: justify;" /><span style="">Ta przeglądarka nie jest przez nas wspierana.<br>Wróć do nas, korzystając z innej przeglądarki.<br>Zalecamy korzystanie z <a href="https://www.google.com/intl/pl_ALL/chrome/" target="_blank">Google Chrome</a></span>');
        //alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    }

});

/**
 * @param {*} date 
 * @return {time, date}
 */

function solveTime(date){

    var getDate = new Date();
        getDate.setTime( (date) * 1000 );

    var onlyTime = resolveTime(getDate),
        onlyDate = resolveDate(getDate);
    
    return {time: onlyTime,date: onlyDate};

}

function resolveTime(date){

    var getHours = date.getHours(),
        getMinutes = date.getMinutes();

    if(getMinutes <= 9){
        getMinutes = '0' + getMinutes; 
    }

    return {hours: getHours, minutes: getMinutes};

}

function resolveDate(date){
    
    var getDate = date.getDate(),
        getMonth = date.getMonth(),
        getMonths = ["stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia"];

    return {date: getDate, month: getMonths[getMonth]};

}

function openNav() {
    var nav = $("nav");
    nav.css({"box-shadow":"0 0 0 1000vw var(--strong-fade)"});
    nav.css({"left": "0"});
}

function closeNav() {
    var nav = $("nav");
    nav.css({"box-shadow":"0 0 0 1000vw rgba(0,0,0,0)"});
    nav.css({"left": "-100%"});
}

function countDown(time){

    var local = new Date(),
        localTime = Math.round(local.getTime() / 1000),
        distance;
    
    if( time > localTime ){
        distance = time - localTime;
    } else if ( time < local.getTime() ){
        distance = localTime - time;
    }
        
    var hours   = Math.floor(distance / 3600);
    var minutes = Math.floor((distance - (hours * 3600)) / 60);
    var seconds = distance - (hours * 3600) - (minutes * 60);

    var response = hours + "g " + minutes + "m";
    
    //console.log(hours + ":" + minutes + ":" + seconds);

    return response;

}

function prompt(id){
    $('main').append('<div class="prompt happyPrompt" id="' + id + 'Prompt">Don t wory be happy</div>');
    setTimeout( $("#" + id + "Prompt").remove("#" + id), 1000 );
}

var timeDistance = 0;

function superDateCareCenter(date){

    var localDate = new Date();
    
    date = date * 1000;
    
    if( localDate < date ){
    
        var distance = date - localDate;
    
        if( timeDistance > distance ){
        
            timeDistance = distance;

        }
        
    }
    
}