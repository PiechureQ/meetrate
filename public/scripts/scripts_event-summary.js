
function createSpeakersSection(events){

    var result = '<section id="speakers"><h1>Ocena prelegentów</h1><div id="results" class="col-12"><ol>';
        
    for( let i = 0 ; i < events.length ; i++ ){

        var event = events[i]
            data = getTalkData(event);
            rate = generateIcon(data.rate);

        result += '<li id="jeden" class="col-12 col-sm-12 col-md-6"><h1>' + data.speakersStr + '</h1><h2>' + data.name + '</h2>' + rate + '<h4>' + data.ratecount + ' głosów</h4></li>';

    }
        
    result += '</ol></div></section>';

    return result;

}

function createSpeakersRate(avg, count){

    var result = '<section id="SpeakersSummary"><h1>Średnia ocena prelegentów</h1>';

        result += generateIcon(avg);

        result += '</section>';

    return result;

}

function createEventRate(avg, count){

    var divClass = 'neutral';

    if ( count > 0 ) {

        avg = avg.toFixed(1);

        if(avg >= 3.8){
            divClass = 'positive';
        } else if(avg > 2){
            divClass = 'neutral';
        } else if(avg > 1){
            divClass = 'sad';
        }

    } else {

        avg = "brak";

    }

    

    var result = '<section id="eventSummary"><h1>Ocena wydarzenia</h1>';
        result += '<div class="' + divClass + '">' + avg + "</div><h2> w skali od 1 do 5 </h2><h4>" + count + ' głosów</h4>';
        result += '</section>';

    return result;

}

function generateIcon(rate){

    var imgSrc,
        divClass;

    rate = Math.round(rate);

    switch (rate){
        case 3:
            imgSrc = 'rate-positive.svg';
            divClass = 'positive';
            rate = "Super!";
            break;
        case 2:
            imgSrc = 'rate-neutral.svg';
            divClass = 'neutral';
            rate = 'Poprawnie';
            break;
        case 1:
            imgSrc = 'rate-sad.svg';
            divClass = 'sad';
            rate = 'Niezadowalająco';
            break;
        default:
            imgSrc = 'logo-sygnet.svg';
            divClass = '';
            rate = "Brak";
    }

    return '<div class="icon-rate ' + divClass + '"><img src="img/' + imgSrc + '" /></div><h3>' + rate + '</h3>';

}
