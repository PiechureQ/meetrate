
function createActiveSpeakersRate(events){

    var result = '<section id="speakersRate"><h1>Ocena prelegentów</h1><div class="row"><ol class="col-12">';
    
        for( let i = 0 ; i < events.length ; i++ ){

            var event = events[i]
                data = getTalkData(event),
                status = whatsStatus(data.start, data.end),
                bar = null,
                more = null;

            switch(status) {
                case "future":
                    bar = "";
                    more = '<div class="more">';
                    break;
                case "active":
                    bar = '<button onclick="actualFeedbackShow(\'giveRate\', \'' + data.id + '\')">Oceń</button><button onclick="actualFeedbackShow(\'askQuestion\', \'' + data.id + '\')">pytanie</button>';
                    rate = createRateOption(data.id);
                    question = createOpinionOption(data.id);
                    more = '<div class="more"><div class="giveRate feedbackOption">' + rate + '</div><div class="askQuestion feedbackOption">' + question + '</div>';
                    break;
                case "archive":
                    bar = '<button onclick="actualFeedbackShow(\'giveRate\', \'' + data.id + '\')">Oceń</button>';
                    rate = createRateOption(data.id);
                    more = '<div class="more"><div class="feedbackOption giveRate">' + rate + '</div>';
                    break;
                default:
                    bar = "error";
                    more = "ERROR!";
            }

            more += '<button onclick="actualFeedbackHide()" class="hideButton">Schowaj</button></div>';

            var solvedStart = solveTime(data.start);

            result += '<li id="' + data.id + '" class="' + status + '">';
            result += '<h1>' + data.speakersStr + '</h1><h2>' + data.name + '</h2>';
            result += '<div class="date"><i class="icon-icon-clock"></i>' + solvedStart.time.hours + ":" + solvedStart.time.minutes + '</div>';
            result += '<div class="bar">' + bar + '</div>' + more + '';
            result += '</li>';

        }

        result += '</ol></div></section>';

    return result;

}

function createActiveEventRateSection(rawStart, rawEnd, eventid){

    var date = new Date(),
        now = Math.round( date.getTime() / 1000 );
        start = solveTime(rawStart),
        status = whatsStatus(rawStart, rawEnd);


    var result = '<section id="eventRate"><h1>Ocena wydarzenia</h1>';

    if( ( now > rawStart ) && ( now < rawEnd ) ){

        result += '<div class="row eventRadio">';

        for( let i = 1 ; i < 6 ; i++ ){
            result += '<label><input type="radio" name="eventRate" value="' + i + '"><div>' + i + '</div></label>';
        }

        result += '<h2>Jestem zawiedziony</h2><h2>Super!</h2>';
        result += '</div><h1>napisz co możemy poprawić, usprawnić LUB COś od siebie</h1>';
        result += '<textarea maxlength="500"></textarea>';
        result += '<button class="submit" onClick="rateEvent(\'' + eventid + '\')">Prześlij ocenę</button>';
        result += '</div>';

    } else {

        result += '<h1 class="futureSpeaker"><i class="icon-icon-clock"></i>' + start.time.hours + ":" + start.time.minutes + '<br>Możliwość oceny pojawi się po rozpoczęciu wydarzenia.</h1></section>';

    }
    
    result += '</section>';

    return result;

}

function createRateOption(id){
    
    var result = '<h1>Oceń prelekcję</h1>';
        result += '<div class="RateRadio">';
        result += '<label><input type="radio" name="speakerRate" value="1"><div class="img"><img src="img/rate-sad.svg" /></div><h2>Jestem<br>zawiedziony</h2></label>';
        result += '<label><input type="radio" name="speakerRate" value="2"><div class="img"><img src="img/rate-neutral.svg" /></div><h2>Poprawnie</h2></label>';
        result += '<label><input type="radio" name="speakerRate" value="3"><div class="img"><img src="img/rate-positive.svg" /></div><h2>Super!</h2></label>';
        result += '</div><h1>Opinia dla prelegenta</h1>';
        result += '<textarea maxlength="500"></textarea>';
        result += '<button class="submit" onclick="rateSpeaker(\'' + id + '\')">Wyślij</button>';

    return result;

}

function createOpinionOption(id){
    
    var result = '<h1>Zadaj pytanie prelegentowi</h1>';
        result += '<textarea maxlength="500"></textarea>';
        result += '<button class="submit" onClick="askSpeaker(\'' + id + '\')">Wyślij</button>';

    return result;

}

function whatsStatus(start, end){

    var date = new Date(),
        dateSec = Math.round( date.getTime() / 1000 ),
        result;
    
    if ( ( dateSec > start ) && ( dateSec > end ) ) {
        result = "archive";
    } else if ( ( dateSec >= start ) && ( dateSec <= end ) ) {
        result = "active";
    } else if ( ( dateSec < start ) && ( dateSec < end ) ) {
        result = "future";
    } else {
        result = "error"
    }

    return result;

}

function actualFeedbackShow(who, id){

    actualFeedbackHide();
    var statusClass = $('#' + id).attr("class"),
        color;

    switch(statusClass) {
        case "archive":
            color = 'var(--gray)';
            break;
        case "active":
            color = 'var(--green)';
           break;
        case "future":
            color = 'var(--red)';
            break;
        default:
            color = 'var(--orange)';
    }

    $('#' + id + " .more ." + who).show();
    $('#' + id + " .bar").slideUp("slow");
    $('#' + id + " .more").slideDown("slow");
    $('#' + id).css({"border-color": color, "background": "var(--white)"});

}

function actualFeedbackHide(id){

    $(".bar").slideDown("slow");
    $(".more").slideUp("slow");
    $(".feedbackOption").slideUp("slow");
    $('li').css({"border-color": "var(--white)", "background": "var(--gradient-white)"});

}
