
function informationWindow(talks, about, image, start, end, city, place, lat, lng){

    var result = "";

        result += createAboutSection(image, about);
        result += createDateSection(start);
        result += createPlaceSection(city, place, lat, lng);
        result += createAgendaSection(talks, start, end);

    return result;

}

function createAboutSection(img, about){

    let result = '<section id="about"><h1>o wydarzeniu</h1><div class="row">';
        result += '<div class="col-12 col-sm-6"><img src="' + img + '" /></div><div class="col-12 col-sm-6">' + about + '</div>';
        result += '</div></section>';

    return result;
}

function createDateSection(start){

    solvedStart = solveTime(start)

    let result = '<section id="date"><h1>data i godzina</h1><div class="row">';
        result += '<div class="col-12 col-sm-6 dateBlock"><i class="icon-icon-calendar "></i><h1>' + solvedStart.date.date + '</h1> <h2>' + solvedStart.date.month + '</h2></div>';
        result += '<div class="col-12 col-sm-6 dateBlock"><i class="icon-icon-clock"></i><h1>' + solvedStart.time.hours + ":" + solvedStart.time.minutes + '</h1></div>';
        result += '</div></section>';

    return result;

}

function createPlaceSection(city, place, lat, lag){

    let result = '<section id="place"><h1>miejsce</h1><div class="row">';
        result += '<div class="col-6"><h2>' + city + ',</h2><h2>' + place + '</h2></div><p class="col-6"><a href="https://www.google.com/maps?q=' + lat + ', ' + lag + '" target="_blanc"><i class="icon-icon-map"></i>mapa</a></p>';
        result += '</div><div id="map" class="col-12"></div></section>';

    return result;

}

function createAgendaSection(events , eventStart, eventEnd){

    var solvedEventStart = solveTime(eventStart);
    var solvedEventEnd = solveTime(eventEnd);

    timeDistance = eventStart;

    superDateCareCenter(eventEnd);

    var result = '<section id="agenda"><h1>agenda</h1>';
        result +='<ul><dl><dt><i class="icon-icon-clock"></i></dt></dl>';

    result += '<dl><dd><hr></dd><dt>';

    if( ( eventEnd - eventStart ) > 43200 ){
        result += solvedEventStart.date.date + " " + solvedEventStart.date.month + " &#183; ";
    }

    result += solvedEventStart.time.hours + ":" + solvedEventStart.time.minutes + '</dt></dl><li>';
    result += "<h1>Start wydarzenia</h1>";
    result += '</li>';

    for (i = 0; i < events.length; i++) {

        var event = events[i]
            data = getTalkData(event);

        var solvedStart = solveTime(data.start);

        superDateCareCenter(data.start);
        superDateCareCenter(data.end);
        
        result += '<dl><dd><hr></dd><dt>'

        if( ( eventEnd - eventStart ) > 43200 ){
            result += solvedStart.date.date + " " + solvedStart.date.month + " &#183; ";
        }
            
        result += solvedStart.time.hours + ":" + solvedStart.time.minutes + '</dt></dl><li>';

        result += "<h1>" + data.speakersStr + "</h1><h2>" + data.name + "</h2>";
        result += '</li>';

    }

    result += '<dl><dd><hr></dd><dt>';

    if( ( eventEnd - eventStart ) > 43200 ){
        result += solvedEventEnd.date.date + " " + solvedEventEnd.date.month + " &#183; ";
    }

    result += solvedEventEnd.time.hours + ":" + solvedEventEnd.time.minutes + '</dt></dl><li>';
    result += "<h1>Koniec wydarzenia</h1>";
    result += '</li>';

    result += "</ul></section>";
    
    setTimeout(function(){ location.reload(); }, timeDistance);

    return result;

}
		
