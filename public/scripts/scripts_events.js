
function eventGenerate(result){

    const DATA = result,
        ServerTime = DATA.serverTime,
        EventId = DATA._id,
        EventStatus = DATA.status,
        EventName = DATA.name,
        EventLocation = DATA.location,
        EventCity = EventLocation.city,
        EventPlace = EventLocation.place,
        EventLAT = EventLocation.lat,
        EventLNG = EventLocation.lng,
        EventDate = DATA.date,
        EventStart = EventDate.start,
        EventEnd = EventDate.end,
        AboutEvent = DATA.info,
        EventLogo = DATA.img,
        TalksList = DATA.talks,

        EventFeedback = DATA.feedback,
        EventAvg = EventFeedback.avg,
        EventCount = EventFeedback.count,
        TalksFeedback = DATA.feedbackTalks,
        TalksAvg = TalksFeedback.avg,
        TalksCount = TalksFeedback.count;

    var DecodeImage = setImage(EventLogo),
        ThemeColor = "white",
        MainStage = $('main');

    switch( EventStatus ) {
        case 'actual':
            ThemeColor = "green",
            actualEvent(TalksList, AboutEvent, DecodeImage, EventEnd, EventStart, EventCity, EventPlace, EventLAT, EventLNG, EventId);
            break;
        case 'future':
            ThemeColor = "orange";
            futureEvent(TalksList, DecodeImage, AboutEvent, EventStart, EventEnd, EventCity, EventPlace, EventLAT, EventLNG);
            break;
        case 'archive':
            ThemeColor = "gray";
            archiveEvent(TalksList, EventAvg, EventCount, TalksAvg, TalksCount, DecodeImage, AboutEvent, EventStart, EventEnd, EventCity, EventPlace, EventLAT, EventLNG);
            break;
        default:
            console.log("error: unknown event STATUS!");
    }

    setHeader(ThemeColor, EventName, EventCity, EventPlace, EventStart);
    createMap(EventLAT, EventLNG);

}

function getTalkData(talk){

    const event = talk,
        TalkId = event._id,
        TalkName = event.title,
        SPEAKERS = event.speakers,
        DATES = event.date,
        TalkStart = DATES.start,
        TalkEnd = DATES.end,
        FEEDBACK = event.avg,
        TalkRateAvg = FEEDBACK.avg,
        TalkRateCount = FEEDBACK.count;

    var speakersObject = [],
        speakersString = "";

    for( var i = 0 ; i < SPEAKERS.length ; i++ ){

        speakersObject[i] = SPEAKERS[i].name;
        speakersString += SPEAKERS[i].name;
        if( ( SPEAKERS.length > 0 ) && ( SPEAKERS.length != ( i + 1 ) ) ){
            speakersString += ", ";
        }
    }

    return {
        id: TalkId,
        name: TalkName,
        speakersObj: speakersObject,
        speakersStr: speakersString,
        start: TalkStart,
        end: TalkEnd,
        rate: TalkRateAvg,
        ratecount: TalkRateCount
    };

}

function actualEvent(events, about, image, end, start, city, place, lat, lag, id){
    
    var where = $("main"),
        tabs = {results: "oceń",information: "informacje"};

    where.append( createTabs( tabs ) );

    $("#resultsWindow").append( createActiveSpeakersRate(events) );
    $("#resultsWindow").append( createActiveEventRateSection(start, end, id) );

    $("#informationWindow").append( informationWindow(events, about, image, start, end, city, place, lat, lag) );

    $('#resultsTab').click();

    $(".submit").click( function(){
        $(".submit").prop("disabled", true);
        $("textarea").prop("disabled", true);
        $("input[type='radio']").prop("disabled", true);
    });

}

function futureEvent(events, image, about, start, end, city, place, lat, lag){

    $("main").append( informationWindow(events, about, image, start, end, city, place, lat, lag) );

}

function archiveEvent(events, EventAvg, EventCount, TalksAvg, TalksCount, image, about, start, end, city, place, lat, lag){

    var where = $("main"),
        tabs = { results: "wyniki", information: "informacje" };

    where.append( createTabs( tabs ) );
    
    $("#resultsWindow").append( createSpeakersSection(events) );
    $("#resultsWindow").append( createSpeakersRate(TalksAvg, TalksCount) );
    $("#resultsWindow").append( createEventRate(EventAvg, EventCount) );

    $("#informationWindow").append( informationWindow(events, about, image, start, end, city, place, lat, lag) );

    $('#resultsTab').click();

}

function createTabs(names){

    var keys = Object.keys(names),
        result = '<nav id="tabs" class="col-12 col-sm-12">',
        window = '';

    for ( let i = 0 ; i < keys.length ; i++ ){
        
        result += '<div class="tab" id="' + keys[i] + 'Tab" onclick="navigationTabs(\'' + keys[i] + '\')">' + names[ keys[i] ] + '</div>';
        window += '<div class="window" id="' + keys[i] + 'Window"></div>';

    }

    result += "</nav>" + window;

    return result;

}

function navigationTabs(who){

    $(".window").hide();
    $(".tab").removeClass("selected");

    $("#" + who + "Window").show();
    $("#" + who + "Tab").addClass("selected");

}

function setHeader(color, name, city, place, start){
    var solvedStart = solveTime(start);
    document.title = name + " - MeetRate";
    $("header").css({"background": "var(--gradient-" + color + ")"});
    $("#title").html(name);
    $("#description").html(city + ", " + place + "<br>" + solvedStart.date.date + " " + solvedStart.date.month + ", godz. " + solvedStart.time.hours + ":" + solvedStart.time.minutes);
}

function createNavigation(names){

    var result = '<nav id="tabs" class="col-12 col-sm-12">',
        window = '';

    for( let i = 0 ; i < names.length ; i++ ){

        result += '<div class="tab" id="' + names[i] + 'Tab" onclick="navigationTabs(\'' + names[i] + '\')">' + names[i] + '</div>';
        window += '<div class="window" id="' + names[i] + '"></div>';

    }

    result += "</nav>" + window;

    return result;

}

function setImage(img){
    
    img = 'data:image/png;base64,' + img;

    return img;

}
