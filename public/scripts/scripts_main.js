
 $.ajax({
    type: "GET",
    url: "http://dev-vote.rst.com.pl/api/v1/event",
    contentType: "application/json; charset=utf-8",
    dataType: 'jsonp',
    statusCode: {
        200: (data) => {
            console.log(data);
            events(data);
        },
    },
});

function events(result){

    timeDistance = 300000;

    result.pop();

    var data = result,
        time = data.serverTime,
        events = data.events;

    //var serverDate = new Date(time.slice(0, time.length - 1) + "+02:00");

    $('main').html('<section id="actual"><h1>AKTUALNE</h1></section><section id="future"><h1>NADCHODZĄCE</h1></section><section id="archive"><h1>ZAKOŃCZONE</h1></section>');

    for ( let i = 0 ; i < result.length ; i++) {

        var event = data[i],
            id = event._id,
            name = event.name,
            status = event.status,
            location = event.location,
            city = location.city,
            place = location.place,
            lat = location.lat,
            lag = location.lng,
            date = event.date,
            start = date.start,
            end = date.end,
            feedback = event.feedback,
            feedbackTalks = event.feedbackTalks,
            eventRate = feedback.avg,
            eventCount = feedback.count,
            speakersRate = feedbackTalks.avg;
            speakersCount = feedbackTalks.count;

        var solvedEnd = solveTime(end);
        var solvedStart = solveTime(start);

        superDateCareCenter(start);
        superDateCareCenter(end);

        generate(id, name, city, place, status, solvedStart);
        
        if (status == "actual" ) {
            var link = 'https://www.google.com/maps?q=' + lat + ', ' + lag;
            actual(id, end, link);
        } else if ( status == "future" ) {
            future(id);
        } else if ( status == "archive" ) {
            archive(id, eventRate, speakersRate, eventCount, speakersCount);
        }

    }

    var statuses = ["actual", "future", "archive"];

    for( let i = 0 ; i < statuses.length ; i++ ){

        var length = $("#" + statuses[i] + " .eventbox").length;
    
        if ( length == 0 ) {
            $("#" + statuses[i] ).html("");
        }

    }

    setTimeout(function(){ document.location.reload(true); }, timeDistance);

}

function generate(id, name, city, place, status, start){

    $('#' + status).append('<a href="' + id + '"><article id="' + id + '" class="eventbox eventbox-' + status + '"></article>');
    $('#' + id).append('<h1>' + name + '</h1>');
    $('#' + id).append('<h2>' + city + ", " + place + '</h2>');
    $('#' + id).append('<div class="date"><img src="img/calendar-bg.svg" /><h3>' + start.date.date + "</h3>" + start.date.month + '</div></a>');

}

function actual(id, time, link){

    var timeleft = countDown(time);
    timer(id,time);

    $('#' + id).append('<div class="bar"></div>');
    $('#' + id + " .bar").append('<div class="element"><h3><a href="' + link + '" target="_blanc"><i class="icon-icon-map"></i>Mapa</a></h3><h3>Pozostało: <span id="timeleft' + id + '">' + timeleft + '</span></h3></div>');
}

function future(id){

}

function archive(id, eventRate, speakersRate, eventCount, speakersCount){
    
    var speakers = speakersRate,
        target = $('#' + id)

    if( ( eventCount > 0 ) && ( speakersCount > 0 ) ) {

        target.append('<div class="bar"></div>');

    }

    if( speakersCount > 0 ){

        //$('#' + id + " .bar").append('<div class="element"><h3>Prelegenci</h3><h3>' + speakers + '</h3></div>');
    
    }

    if( eventCount > 0 ){

        if( $.isNumeric( eventRate ) ){

            eventRate = eventRate.toFixed(0);

        } else {

            eventRate = 0;

        }

        switch( speakers ) {
            case '1':
                speakers = '<img src="img/rate-positive.svg" />';
                break;
            case '2':
                speakers = '<img src="img/rate-positive.svg" />';
                break;
            case '3':
                speakers = '<img src="img/rate-positive.svg" />';
                break;
            default:
                speakers = speakersRate + "/3";
        }

        $('#' + id + " .bar").append('<div class="element"><h3>Wydarzenie</h3><h3><span>' + eventRate + '</span>/5</h3></div>');

    }
    
}

function timer(id, time){

    setInterval(function(){ 
        
        var timeleft = countDown(time);

        $("#timeleft" + id).html(timeleft);

    }, 1000);

}

function sendMail(){

    var about = $("#sign textarea").val();
    var mail = $("#sign #mail").val();

    var data ={
        email: mail,
        content: about
    };
//todo
    $.ajax({
        type: "POST",
        url: "/api/v1/contact/sendMail",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        processData: false,
        data: JSON.stringify(data),  
        statusCode:{
            200: () => {
                $("#sign .modal-body button").prop("disabled", true);
                $("#sign .modal-body input").prop("disabled", true);
                $("#sign .modal-body textarea").prop("disabled", true);

                $("#sign .modal-footer").show();
                $("#sign .modal-footer").html("Dziękujemy za zgłoszenie!");
            },
            400: () => {
                $("#sign .modal-footer").show();
                $("#sign .modal-footer").html("Błąd serwera.");
            },
            500: () => {
                $("#sign .modal-footer").show();
                $("#sign .modal-footer").html("Nieprawidłowy adres email.");
            }
        }
    })

}